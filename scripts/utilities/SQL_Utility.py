import mysql.connector
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import requests
import json
import datetime
import pytz
import psycopg2, logging


class SQL_Utility(object):
    def __init__(self, db_host, sqldb_port, sqldb_database, sqldb_username, sqldb_password):
        """
                Initializes the Class and gets the configurational values from Application Yaml File
                """
        try:
            self.host = db_host
            self.port = sqldb_port
            self.database = sqldb_database
            self.user = sqldb_username
            self.password = sqldb_password
            self.conn = mysql.connector.connect(host=self.host, port=self.port, database=self.database, user=self.user,
                                                password=self.password)
            # self.conn.autocommit = True
            self.cur = self.conn.cursor()
        except Exception as e:
            logging.exception("Exception found in initialization" + str(e))

    def execute_query(self, query):
        """
        Executes the query and gets you the result
        :param query:
        :return:
        """
        try:
            print("query", query)
            with self.conn.cursor() as cursor:
                row_count = cursor.execute(str(query))
                print(row_count)
                result = cursor.fetchall()
                result_list = []
                for each_result in result:
                    result_list.append(each_result)
            return result_list
        except Exception as e:
            logging.exception("Exception found in initialization" + str(e))

    def update_data(self, query, value):
        """
                Executes the query and gets you the result
                :param query:
                :return:
                """
        try:
            self.cur.execute(query, value)
            self.conn.commit()
            return True
        except Exception as e:
            logging.exception("Exception found in initialization" + str(e))
            return False
