from scripts.constants.app_configuration import app_config
from scripts.constants.app_constants import SQL
from scripts.logging import logger_util
from scripts.utilities.SQL_Utility import SQL_Utility

LOG = logger_util.get_logger()


class Login(object):

    def __init__(self):
        """
        Initialises the constructor
        """
        self.db_obj = SQL_Utility(db_host=app_config[SQL.config_section][SQL.host],
                                  sqldb_port=app_config[SQL.config_section][SQL.port],
                                  sqldb_database=app_config[SQL.config_section][SQL.database],
                                  sqldb_username=app_config[SQL.config_section][SQL.username],
                                  sqldb_password=app_config[SQL.config_section][SQL.password])

    def calculation_handler(self, input_data):
        """
        The Purpose of this Code is to send the data to Data Base and store
        """
        final_json = {"status": "failed", "message": "failed"}
        try:
            # Variables
            shift_model = input_data["shift_model"]
            gross_shift_time = input_data["gross_shift_time"]
            total_break_time = input_data["total_break_time"]
            start_up_time = input_data["start_up_time"]
            break_time = input_data["break_time"]
            lunch_time = input_data["lunch_time"]
            other_time = input_data["others"]
            standard_shift_time = input_data["standard_shift_time"]
            days_per_week = input_data["days_per_week"]
            weeks_per_year = input_data["days_per_week"]
            query = """
            INSERT INTO umlaut (shift_model, gross_shift_time, total_break_time, start_up_time, break_time, 
            lunch_time, other_time, standard_shift_time, days_per_week, weeks_per_year)
            
            VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
            """
            value = (shift_model, gross_shift_time, total_break_time, start_up_time, break_time, lunch_time,
                     other_time, standard_shift_time, days_per_week, weeks_per_year)
            status = self.db_obj.update_data(query, value)
            if status is True:
                final_json["message"] = "Data Inserted Successfully"
                final_json["status"] = "success"
            else:
                final_json["message"] = "Something Went Wrong during Insertion"
        except Exception as e:
            LOG.error("Exception in Insertion")
            LOG.error(e)
        return final_json

    def data_display_handler(self):
        """
        The Purpose of this Code is to Display the data back to the JavaScript Code
        """
        final_json = {"status": "failed", "message": "failed", "data": dict()}
        try:
            query = """SELECT shift_model, gross_shift_time, total_break_time, start_up_time, break_time,lunch_time, other_time, standard_shift_time, days_per_week, weeks_per_year from umlaut"""
            data = self.db_obj.execute_query(query=query)
            for each_row in data:
                final_json["data"]["shift_model"] = each_row[0]
                final_json["data"]["gross_shift_time"] = each_row[1]
                final_json["data"]["total_break_time"] = each_row[2]
                final_json["data"]["start_up_time"] = each_row[3]
                final_json["data"]["break_time"] = each_row[4]
                final_json["data"]["lunch_time"] = each_row[5]
                final_json["data"]["other_time"] = each_row[6]
                final_json["data"]["standard_shift_time"] = each_row[7]
                final_json["data"]["days_per_week"] = each_row[8]
                final_json["data"]["weeks_per_year"] = each_row[9]
            final_json["status"] = "success"
        except Exception as e:
            LOG.error("Exception in Insertion")
            LOG.error(e)
        return final_json
