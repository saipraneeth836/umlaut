CONFIGURATION_FILE = "conf/application.yaml"

license_definition_list = []


class Api:
    Save = "/save"
    Render = "/render"


class FlaskService:
    GET = "GET"
    POST = "POST"
    method_not_supported = "Method not supported!"
    service_api = "service_api"
    config_section = "FLASK"
    port = "port"


class Log:
    config_section = "LOG"


class SQL:
    port = "port"
    host = "host"
    username = "username"
    password = "password"
    default_id = "_id"
    config_section = "SQL"
    database = "database"
