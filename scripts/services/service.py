import json
import traceback

from flask import Blueprint, request

from scripts.constants.app_constants import FlaskService, Api
from scripts.handlers.login_handler import Login
from scripts.logging import logger_util

LOG = logger_util.get_logger()

serv = Blueprint(FlaskService.service_api, __name__)


@serv.route(Api.Save, methods=['POST'])
def save():
    try:
        if request.method == FlaskService.POST:
            try:
                input_data = request.get_data()
                input_data = json.loads(input_data)
                response = Login().calculation_handler(input_data)
                return response
            except Exception as e:
                LOG.exception(e)
                return {"status": "failed", "message": str(e)}
        else:
            return FlaskService.method_not_supported

    except Exception as e:
        traceback.print_exc()
        LOG.exception(e)
        return {"status": "failed", "message": str(e)}


@serv.route(Api.Render, methods=['GET'])
def render():
    try:
        if request.method == FlaskService.GET:
            try:
                response = Login().data_display_handler()
                return response
            except Exception as e:
                LOG.exception(e)
                return {"status": "failed", "message": str(e)}
        else:
            return FlaskService.method_not_supported

    except Exception as e:
        traceback.print_exc()
        LOG.exception(e)
        return {"status": "failed", "message": str(e)}
