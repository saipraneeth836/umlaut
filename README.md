# Umlaut Project Challenge

> Steps to run the Project

The Main File or the master file of the project is app.py

## Steps to Proceed

- Step-1: Go to db_dump folder and take the dump of the DB and execute in a SQL server local instance.
- Step-2: Execute **pip install -r"requirements.txt"** command in the terminal
- Step -3: Execute app.py and the back-end code runs.

## Input and Output JSONs for REST API Calls using Flask

### Save API
- /save - This will save the data into DB that is sent by the User Interface.
- Input JSON will be: {
"break_time": "0",
"days_per_week": "50",
"gross_shift_time": "480",
"lunch_time": "0",
"other_time": "0",
"shift_model": "2",
"standard_shift_time": "470",
"start_up_time": "10",
"total_break_time": "10",
"weeks_per_year": "50"
}
### Render API
- /render - This will render the data from DB back to User Interface.
- Since its a GET Request there wont be any Input JSON
- Successful Response will give - {
"data": {
"break_time": "0",
"days_per_week": "50",
"gross_shift_time": "480",
"lunch_time": "0",
"other_time": "0",
"shift_model": "2",
"standard_shift_time": "470",
"start_up_time": "10",
"total_break_time": "10",
"weeks_per_year": "50"
},
"message": "Successfully fetched the data",
"status": "success"
}
- Failed Response will give - {
"data": { },
"message": "Failed to fetch the data",
"status": "failed"
}
